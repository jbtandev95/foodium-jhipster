/**
 * View Models used by Spring MVC REST controllers.
 */
package com.joe.foodium.web.rest.vm;
