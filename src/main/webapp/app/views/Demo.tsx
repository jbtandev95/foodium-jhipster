import React from "react";

// reactstrap components
import { Container, Row } from 'reactstrap';

// index page sections
// import Hero from "./IndexSections/Hero.js";
import Buttons from "../modules/demo/buttons";
import Inputs from "../modules/demo/inputs";
import CustomControls from "../modules/demo/customControls";
// import Menus from "./IndexSections/Menus.js";
import Navbars from "../modules/demo/navbars";
import Tabs from "../modules/demo/tabs";
import Progress from "../modules/demo/progress";
import Pagination from "../modules/demo/pagination";
import Pills from "../modules/demo/pills";
import Labels from "../modules/demo/labels";
import Alerts from "../modules/demo/alerts";
import Typography from "../modules/demo/typography";
import Modals from "../modules/demo/modals";
import Datepicker from "../modules/demo/datepicker";
import TooltipPopover from "../modules/demo/toolTipPopover";
import Carousel from "../modules/demo/carousel";
import Icons from "../modules/demo/icons";
import Login from "../modules/demo/login";
import Download from "../modules/demo/download";

class Demo extends React.Component {
  private mainRef: React.RefObject<HTMLInputElement>;

  constructor(props) {
    super(props);
    this.mainRef = React.createRef();
  }
  componentDidMount() {
    document.documentElement.scrollTop = 0;
    document.scrollingElement.scrollTop = 0;
    if (this.mainRef.current) {
      this.mainRef.current.scrollTop = 0;
    }
  }
  render() {
    return (
      <>
        <main ref={this.mainRef}>
          <Buttons />
          <Inputs />
          <section className="section">
            <Container>
              <CustomControls />
              {/* <Menus /> */}
            </Container>
          </section>
          <Navbars />
          <section className="section section-components">
            <Container>
              <Tabs />
              <Row className="row-grid justify-content-between align-items-center mt-lg">
                <Progress />
                <Pagination />
              </Row>
              <Row className="row-grid justify-content-between">
                <Pills />
                <Labels />
              </Row>
              <Alerts />
              <Typography />
              <Modals />
              <Datepicker />
              <TooltipPopover />
            </Container>
          </section>
          <Carousel />
          <Icons />
          <Login />
          <Download />
        </main>
      </>
    );
  }
}

export default Demo;
